<?php

namespace Acme;

interface HoveringCarInterface
{
    public function hover();
}