<?php

namespace Acme;

class CarAdapter implements CarInterface
{
    protected $hoveringCar;
    /**
     * CarAdapter constructor.
     */
    public function __construct(HoveringCarInterface $hoveringCar)
    {
        $this->hoveringCar = $hoveringCar;
    }

    public function drive()
    {
        $this->hoveringCar->hover();
    }
}