<?php
/**
 * Created by PhpStorm.
 * User: Arnoldas
 * Date: 2017-10-26
 * Time: 18:35
 */

namespace Acme;


interface CarInterface
{
    public function drive();
}