<?php

namespace Acme;

class Mediator implements MediatorInterface
{
    /**
     * @var DoorClient
     */
    private $doorClient;

    /**
     * @var DoorLockPassSetter
     */
    private $doorLockPassSetter;

    /**
     * @var DoorAlarmSetter
     */
    private $doorAlarmSetter;

    /**
     * Mediator constructor.
     * @param DoorClient $doorClient
     * @param DoorLockPassSetter $doorLockPassSetter
     * @param DoorAlarmSetter $doorAlarmSetter
     */
    public function __construct(DoorClient $doorClient,
                                DoorLockPassSetter $doorLockPassSetter,
                                DoorAlarmSetter $doorAlarmSetter)
    {
        $this->doorClient = $doorClient;
        $this->doorLockPassSetter = $doorLockPassSetter;
        $this->doorAlarmSetter = $doorAlarmSetter;

        $this->doorClient->setMediator($this);
        $this->doorLockPassSetter->setMediator($this);
        $this->doorAlarmSetter->setMediator($this);
    }

    public function lockDoor()
    {
        $this->doorLockPassSetter->setDoorLockPass();
        $this->doorAlarmSetter->setDoorAlarm();
    }

    /**
     * @param string $content
     */
    public function sendResponse($content)
    {
        $this->doorClient->output($content);
    }
}