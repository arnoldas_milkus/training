<?php

namespace Acme;


interface MediatorInterface
{
    public function lockDoor();

    public function sendResponse($content);
}