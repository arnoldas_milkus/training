<?php

namespace Acme;


class DoorClient extends Colleague
{
    public function lockDoor()
    {
        $this->mediator->lockDoor();
    }

    public function output($content)
    {
        echo $content;
    }
}