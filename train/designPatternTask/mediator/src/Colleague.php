<?php
/**
 * Created by PhpStorm.
 * User: Arnoldas
 * Date: 2017-10-26
 * Time: 19:20
 */

namespace Acme;


abstract class Colleague
{
    /**
     * @var MediatorInterface
     */
    protected $mediator;

    /**
     * @param MediatorInterface $mediator
     */
    public function setMediator(MediatorInterface $mediator)
    {
        $this->mediator = $mediator;
    }
}