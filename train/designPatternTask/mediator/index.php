<?php

require 'vendor/autoload.php';


$client = new \Acme\DoorClient();
new \Acme\Mediator($client, new \Acme\DoorLockPassSetter(), new \Acme\DoorAlarmSetter());

$client->lockDoor();