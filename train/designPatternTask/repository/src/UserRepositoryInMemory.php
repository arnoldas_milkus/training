<?php

namespace Acme;

/**
 * Class UserRepositoryInMemory
 * @package Acme
 */
class UserRepositoryInMemory implements UserRepository
{
    /** @var array User */
    private $users;

    /**
     * UserRepositoryInMemory constructor.
     * @param $users
     */
    public function __construct($users)
    {
        $this->users = $users;
    }

    public function find($id)
    {
        return $this->users[$id];
    }

    public function findUserByUsername($username)
    {
        foreach ($this->users as $user) {
            if ($user->getUsername() === $username) {
                return $user;
            }
        }

        return null;
    }
}