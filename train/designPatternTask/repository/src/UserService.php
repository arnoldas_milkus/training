<?php

namespace Acme;

/**
 * Class UserService
 * @package Acme
 */
class UserService
{
    /** @var UserRepository  */
    private $userRepo;

    /**
     * UserService constructor.
     * @param $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function findUserById($id)
    {
        return $this->userRepo->find($id);
    }

    public function findUserByUsername($username)
    {
        return $this->userRepo->findUserByUsername($username);
    }
}