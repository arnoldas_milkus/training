<?php

namespace Acme;


interface UserRepository {

    public function find($id);

    public function findUserByUsername($username);

}