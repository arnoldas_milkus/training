<?php

require 'vendor/autoload.php';


$users = [
    new \Acme\User(0,"name0"),
    new \Acme\User(1, "name1"),
    new \Acme\User(2, "name2")
];

$userRepo = new \Acme\UserRepositoryInMemory($users);
$userService = new \Acme\UserService($userRepo);

var_dump($userService->findUserById(1));
var_dump($userService->findUserByUsername("name2"));