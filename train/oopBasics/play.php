<?php

require_once __DIR__.'/lib/Ship.php';

/**
 * @param Ship $ship
 */
function printShipSummary($ship) {
	
	$ship->sayHello();
	echo '<hr/>';
	echo 'Ship Name: '.$ship->name;
	echo '<hr/>';
	echo $ship->getName();
	echo '<hr/>';
	echo $ship->getNameAndSpecs(false);
	echo '<hr/>';
	echo $ship->getNameAndSpecs(true);

}

$ship = new Ship();
$ship->name = 'Jedi ship';
$ship->weaponPower = 10;

$newShip = new Ship();
$newShip->name = 'Imperial Shutle';
$newShip->weaponPower = 5;
$newShip->strength = 50;

printShipSummary($ship);
echo '<hr/>';
printShipSummary($newShip);
echo '<hr/>';
if ($ship->doesGivenShipHasMoreStrength($newShip)) {
	echo $newShip->name;
} else {
	echo $ship->name;
}
echo ' has more strength';