<?php

//require 'vendor/autoload.php';

// encapsulate and make then interchangeable

interface Logger
{
    public function log($data);
}

// Define a family of algorithms

class LogToFile implements Logger
{

    public function log($data)
    {
        var_dump('log to file');
    }
}

class LogToDatabase implements Logger
{
    public function log($data)
    {
        var_dump('log to database');
    }
}

class LogToXWebService implements Logger
{
    public function log($data)
    {
        var_dump('log to web service');
    }
}

class App
{
    public function log($data, Logger $logger = null)
    {
        $logger = $logger ? $logger : new LogToFile();
        $logger->log($data);
    }
}

$app = new App();

$app->log('data');