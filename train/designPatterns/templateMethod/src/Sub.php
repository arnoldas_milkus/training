<?php

namespace Acme;


abstract class Sub
{
    public function make()
    {
        return $this
            ->layBread()
            ->addLettuce()
            ->addPrimaryToppings()
            ->addSauces();
    }

    protected function layBread()
    {
        var_dump('lay bread');

        return $this;
    }

    protected function addLettuce()
    {
        var_dump('add lettuce');

        return $this;
    }

    protected function addSauces()
    {
        var_dump('add sauces');

        return $this;
    }

    protected abstract function addPrimaryToppings();
}