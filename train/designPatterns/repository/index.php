<?php


require 'vendor/autoload.php';


$repository = new \Acme\PostRepository(new \Acme\MemoryStorage());
$post = new \Acme\Post(null, 'Repository Pattern', 'Design Patterns PHP');

$repository->save($post);

echo $post->getId() . PHP_EOL;
echo $repository->findById(1)->getId();
