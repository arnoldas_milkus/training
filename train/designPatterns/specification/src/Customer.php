<?php

use Illuminate\Database\Eloquent\Model as Elequent;
class Customer extends Elequent
{
    protected $fillable = ['name', 'type'];

    public function type()
    {
        return $this->type;
    }
}