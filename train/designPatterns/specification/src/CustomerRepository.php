<?php


class CustomerRepository
{


    public function bySpecification($specification)
    {
        $matches = [];
        foreach ($this->all() as $customer) {
            if ($specification->isSatisfieldBy($customer)) {
                $matches = $customer;
            }
        }

        return $matches;
    }

    public function all()
    {
        return Customer::all();
    }
}