<?php

use Illuminate\Database\Capsule\Manager as DB;
class CustomerRepositoryTest extends PHPUnit_Framework_TestCase
{
    protected $customers;
    public function setUp()
    {
        $this->customers = new CustomerRepository();
    }


    /** @test */
    public function it_fetches_all_customer_with_spec()
    {
        $customers = new CustomerRepository(
            [
                new Customer('gold'),
                new Customer('silver'),
                new Customer('bronze'),
                new Customer('gold'),
            ]
        );
        $spec = new CustomerIsGold();

        $results = $customers->bySpecification(new CustomerIsSpec);

        $this->assertCount(2, $results);
    }
}