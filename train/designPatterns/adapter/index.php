<?php

require 'vendor/autoload.php';

use Acme\Book;
use Acme\Nook;
use Acme\eReaderAdapter;
use Acme\BookInterface;

class Person {
    public function read(BookInterface $book)
    {
        $book->open();
        $book->turnPage();
    }
}

(new Person)->read(new eReaderAdapter(new Nook()));