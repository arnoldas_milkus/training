<?php

interface Subject // Publisher
{
    public function attach($observable);

    public function detach($observer);

    public function notify();

}

interface Observer // Subscriber
{
    public function handle();
}

class Login implements Subject
{
    /** @var array Observer */
    protected $observers = [];

    public function attach($observable)
    {
        if (is_array($observable)) {
            return $this->attachObservers($observable);
        }
        $this->observers[] = $observable;

        return $this;
    }

    public function detach($index)
    {
        unset($this->observers[$index]);
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->handle();
        }
    }

    /**
     * @param $observable
     */
    private function attachObservers($observable)
    {
        foreach ($observable as $observer) {
            if ($observer instanceof Observer) {
                $this->attach($observer);
            }
        }

        return;
    }

    public function fire()
    {
        $this->notify();
    }
}

class LogHandler implements Observer
{

    public function handle()
    {
        var_dump('log som');
    }
}


class EmailNotifier implements Observer
{

    public function handle()
    {
        var_dump('fier off email');
    }
}


class LoginReporter implements Observer
{

    public function handle()
    {
        var_dump('do some reporting');
    }
}

$login = new Login();
$login->attach([
    new LogHandler(),
    new EmailNotifier(),
    new LoginReporter()
]);
$login->fire();