<?php

abstract class HomeChecker
{
    /** @var  HomeChecker */
    protected $successor;

    public abstract function check(HomeStatus $home);

    public function setSuccessor(HomeChecker $successor)
    {
        $this->successor = $successor;
    }

    public function next(HomeStatus $home)
    {
        if ($this->successor) {
            $this->successor->check($home);

        }
    }
}

class Locks extends HomeChecker
{
    public function check(HomeStatus $home)
    {
        if (!$home->locked) {
            throw new Exception('This doors are not locked');
        }

        $this->next($home);
    }
}

class Lights extends HomeChecker
{
    public function check(HomeStatus $home)
    {
        if (!$home->lightsOff) {
            throw new Exception('This lights on');
        }


        $this->next($home);
    }
}

class Alarm extends HomeChecker
{
    public function check(HomeStatus $home)
    {
        if (!$home->alarmOn) {
            throw new Exception('This alarm not on');
        }

        $this->next($home);
    }
}

class HomeStatus
{
    public $alarmOn = true;
    public $lightsOff = true;
    public $locked = true;

}

$locks = new Locks();
$lights = new Lights();
$alarm = new Alarm();

$status = new HomeStatus();

$locks->setSuccessor($lights);
$lights->setSuccessor($alarm);

$locks->check($status);