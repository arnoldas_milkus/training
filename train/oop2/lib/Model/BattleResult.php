<?php

/**
 * Class BattleResult
 */
class BattleResult
{
    /** @var Ship  */
    private $winningShip;
    /** @var Ship  */
    private $losingShip;
    /** @var bool  */
    private $userJediPowers;

    /**
     * BattleResult constructor.
     * @param Ship $winningShip
     * @param Ship $losingShip
     * @param boolean $userJediPowers
     */
    public function __construct(Ship $winningShip = null, Ship $losingShip = null, $userJediPowers)
    {
        $this->winningShip = $winningShip;
        $this->losingShip = $losingShip;
        $this->userJediPowers = $userJediPowers;
    }

    /**
     * @return Ship|null
     */
    public function getWinningShip()
    {
        return $this->winningShip;
    }

    /**
     * @return Ship|null
     */
    public function getLosingShip()
    {
        return $this->losingShip;
    }

    /**
     * @return boolean
     */
    public function wereJediPowersUsed()
    {
        return $this->userJediPowers;
    }

    /**
     * @return bool
     */
    public function isThereAWinner()
    {
        return $this->getWinningShip() !== null;
    }
}
