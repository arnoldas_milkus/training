<?php

namespace oop2\Controller;

use oop2\Service\RegistrationContainer;
use oop2\Model\User;

/**
 * Class RegistrationController
 * @package oop2\Model
 */
class RegistrationController
{
    /** @var int */
    private $minPassLength = 6;
    /** @var RegistrationContainer */
    private $container;

    /**
     * RegistrationController constructor.
     */
    public function __construct()
    {
        $this->container = new RegistrationContainer();
    }

    /**
     * @param string $username
     * @param string $pass
     * @return string
     */
    public function registerUser($username, $pass)
    {
        $message = $this->validatePassword($pass, $this->minPassLength);

        if (strlen($message) === 0) {
            $userService = $this->container->getUserService();
            $pass = $userService->encryptPassword($pass);
            $user = $this->createUser($username, $pass);
            $userService->registerUser($user);
            $message = 'User registered successfully';
        }

        return $message;
    }

    /**
     * @param string $username
     * @param string $pass
     * @return User
     */
    private function createUser($username, $pass)
    {
        $user = new User();
        $user->setId(microtime())->setUsername($username)->setPassword($pass);

        return $user;
    }

    /**
     * @param string $pass
     * @param int $minPassLength
     * @return string
     */
    private function validatePassword($pass, $minPassLength)
    {
        $message = '';
        $validationService = $this->container->getValidationService();

        if (!$validationService->doesStringContainLowerLetter($pass)) {
            $message .= 'Password has no lower letter' . PHP_EOL;
        }
        if (!$validationService->doesStringContainUpperLetter($pass)) {
            $message .= 'Password has no upper letter' . PHP_EOL;
        }
        if (!$validationService->doesStringContainNumber($pass)) {
            $message .= 'Password has no number' . PHP_EOL;
        }
        if (!$validationService->isStringLengthGreaterThan($pass, $minPassLength)) {
            $message .= 'Password must be greater than ' . $minPassLength . PHP_EOL;
        }

        return $message;
    }
}