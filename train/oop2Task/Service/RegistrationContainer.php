<?php

namespace oop2\Service;

/**
 * Class RegistrationContainer
 * @package oop2\Service
 */
class RegistrationContainer
{
    /** @var  UserService */
    private $userService;
    /** @var  ValidationService */
    private $validationService;

    /**
     * @return UserService
     */
    public function getUserService()
    {
        if ($this->userService === null) {
            $this->userService = new UserService();
        }

        return $this->userService;
    }

    /**
     * @return ValidationService
     */
    public function getValidationService()
    {
        if ($this->validationService === null) {
            $this->validationService = new ValidationService();
        }

        return $this->validationService;
    }
}