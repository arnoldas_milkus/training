<?php

namespace oop2\Service;

use oop2\Model\UserInterface;

/**
 * Class UserService
 * @package oop2\Service
 */
class UserService
{
    /**
     * @param string $password
     * @return string
     */
    public function encryptPassword($password)
    {
        return sha1($password);
    }

    /**
     * @param UserInterface $user
     */
    public function registerUser($user)
    {
        $user->setEnabled(true);
    }
}