<?php

namespace oop2\Service;

/**
 * Class ValidationService
 * @package oop2\Service
 */
class ValidationService
{
    /**
     * @param string $string
     * @param int $minValue
     * @return bool
     */
    public function isStringLengthGreaterThan($string, $minValue)
    {
        return strlen($string) > $minValue;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function doesStringContainUpperLetter($string)
    {
        if (preg_match('/[A-Z]/', $string)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function doesStringContainLowerLetter($string)
    {
        if (preg_match('/[a-z]/', $string)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function doesStringContainNumber($string)
    {
        if (preg_match('/[0-9]/', $string)) {
            return true;
        }

        return false;
    }
}