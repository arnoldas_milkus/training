<?php

namespace oop2\Model;

/**
 * Interface UserInterface
 * @package oop2\Model
 */
interface UserInterface
{
    /**
     * @return string
     */
    public function getId();
    /**
     * @return string
     */
    public function getUsername();
    /**
     * @return string
     */
    public function getPassword();
    /**
     * @return bool
     */
    public function isEnabled();
    /**
     * @param $id
     * @return $this
     */
    public function setId($id);
    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username);
    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password);
    /**
     * @param $enabled
     * @return $this
     */
    public function setEnabled($enabled);
}