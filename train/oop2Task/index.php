<?php

require_once __DIR__ . '/Controller/RegistrationController.php';
require_once __DIR__ . '/Model/User.php';
require_once __DIR__ . '/Model/UserInterface.php';
require_once __DIR__ . '/Service/RegistrationContainer.php';
require_once __DIR__ . '/Service/UserService.php';
require_once __DIR__ . '/Service/ValidationService.php';

$message = '';
if (isset($_POST['username']) && isset($_POST['password'])) {
    $pass = $_POST['password'];
    $username = $_POST['username'];

    $regController = new \oop2\Controller\RegistrationController();
    $message = $regController->registerUser($username, $pass);
}
?>
<html>
<body>
<form method="POST" action="/index.php">
    <?php if ($message): ?>
        <div>
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <label for="username">Username</label>
    <input type="text" name="username" id="username"/>
    <label for="password">Password</label>
    <input type="password" name="password" id="password"/>
    <button type="submit">Register</button>
</form>
</body>
</html>